# App mailape (local)
From the book "Building-Django-2.0-Web-Applications" by Tom Aratyn

## Description
For reference of chapters 9 - 12 about app "Mail ape" on local environment, Some codes are modified, since the codes in book may be outdated or not work

Codes are not by separate chapters; Shall follow the book's content for code sequence

Additional programs required: 
- Redis (for windows, https://github.com/microsoftarchive/redis/releases/) 
- cURL (for Windows, https://curl.haxx.se/download.html)

